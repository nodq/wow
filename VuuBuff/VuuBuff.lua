-- BUFFS AND DEBUFF SIZE

local LARGE_AURA_SIZE = 22 -- Auras you applied to target/focus. Default size 19
local SMALL_AURA_SIZE = 18 -- Auras others applied to target/focus. Default size 17
local AURA_OFFSET_Y = 3
hooksecurefunc("TargetFrame_UpdateAuraPositions", function(self, auraName, numAuras, numOppositeAuras, largeAuraList, updateFunc, maxRowWidth, offsetX, mirrorAurasVertically)
-- a lot of this complexity is in place to allow the auras to wrap around the target of target frame if it's shown
-- Position auras
local size;
local offsetY = AURA_OFFSET_Y;
-- current width of a row, increases as auras are added and resets when a new aura's width exceeds the max row width
local rowWidth = 0;
local firstBuffOnRow = 1;
for i=1, numAuras do
-- update size and offset info based on large aura status
if ( largeAuraList[i] ) then
size = LARGE_AURA_SIZE;
offsetY = AURA_OFFSET_Y + AURA_OFFSET_Y;
else
size = SMALL_AURA_SIZE;
end
-- anchor the current aura
if ( i == 1 ) then
rowWidth = size;
else
rowWidth = rowWidth + size + offsetX;
end
if ( rowWidth > maxRowWidth ) then
-- this aura would cause the current row to exceed the max row width, so make this aura
-- the start of a new row instead
updateFunc(self, auraName, i, numOppositeAuras, firstBuffOnRow, size, offsetX, offsetY, mirrorAurasVertically);
rowWidth = size;
firstBuffOnRow = i;
offsetY = AURA_OFFSET_Y;
else
updateFunc(self, auraName, i, numOppositeAuras, i - 1, size, offsetX, offsetY, mirrorAurasVertically);
end
end
end)