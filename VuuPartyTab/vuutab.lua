local f = CreateFrame("Button","TabParty",nil,"SecureActionButtonTemplate")
f:SetAttribute("type","target")
SecureHandlerWrapScript(f,"OnClick",f,[[
  local unit
  for i=1,4 do
    TabPartyIndex = (TabPartyIndex or 0)%4+1
    unit = "party"..TabPartyIndex
    if UnitExists(unit) then
      self:SetAttribute("unit",unit)
      break
    end
  end
]])
